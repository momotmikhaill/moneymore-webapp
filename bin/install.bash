echo "Installing of high-level dependencies"
npm ci

echo "Installing of client dependencies"
cd client && npm ci && cd ../

echo "Installing of backend dependencies"
cd backend && npm ci && cd ../

echo "Dependencies have been installed"