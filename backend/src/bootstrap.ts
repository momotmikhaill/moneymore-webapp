import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ConsoleLogger, ValidationPipe } from '@nestjs/common';
import { IAppConfig } from './configuration/app.config';

const GLOBAL_PREFIX = 'api';

export default async function bootstrap() {
  const logger = new ConsoleLogger();
  const app = await NestFactory.create(AppModule, { logger });

  /** CONFIGURATION **/
  const config: ConfigService = app.get(ConfigService);
  const appConfig = config.get<IAppConfig>('app');
  app.setGlobalPrefix(GLOBAL_PREFIX);
  app.useGlobalPipes(new ValidationPipe({ transform: true, forbidUnknownValues: true }));

  await app.listen(appConfig.port);
  logger.log(`Application has been started on port ${appConfig.port}`, bootstrap.name);
}
