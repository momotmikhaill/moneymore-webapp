import { registerAs } from '@nestjs/config';

export interface IAppConfig {
  port: number;
}

export default registerAs(
  'app',
  (): IAppConfig => ({
    port: process.env.APP_PORT ? Number(process.env.APP_PORT) : 7777,
  }),
);
