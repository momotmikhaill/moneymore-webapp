import { registerAs } from '@nestjs/config';

export interface IJWTConfig {
  secret: string;
  expireIn: number;
}

export default registerAs(
  'jwt',
  (): IJWTConfig => ({
    secret: process.env.JWT_SECRET,
    expireIn: process.env.JWT_EXPIRE_IN ? Number(process.env.JWT_EXPIRE_IN) : 3600,
  }),
);
