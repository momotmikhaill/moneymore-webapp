import { registerAs } from '@nestjs/config';

export interface IDatabaseConfig {
  host: string;
  prefix: string;
  name: string;
  username: string;
  password: string;
}

export default registerAs(
  'database',
  (): IDatabaseConfig => ({
    host: process.env.DATABASE_HOST,
    prefix: process.env.DATABASE_PREFIX,
    name: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
  }),
);
