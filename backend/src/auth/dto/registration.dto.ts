import { Transform, TransformFnParams } from 'class-transformer';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';

export class RegistrationDto {
  @Transform(({ value }: TransformFnParams) => (value ? value.trim() : ''))
  @IsString()
  @MaxLength(255)
  name: string;

  @Transform(({ value }: TransformFnParams) => (value ? value.trim() : ''))
  @IsEmail()
  email: string;

  @Transform(({ value }: TransformFnParams) => (value ? value.trim() : ''))
  @IsString()
  @MinLength(6)
  @MaxLength(255)
  password: string;
}
