import { ConsoleLogger, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JWTGuard extends AuthGuard('jwt') {
  logger = new ConsoleLogger(JWTGuard.name);

  handleRequest(err: Error, user, info: Error): any {
    if (err) {
      this.logger.error(err);
      throw new UnauthorizedException();
    }

    if (info) {
      this.logger.debug(info);
      throw new UnauthorizedException();
    }

    if (!user) {
      throw new UnauthorizedException('User not found');
    }

    return user;
  }
}
