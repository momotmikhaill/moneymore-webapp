import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { RegistrationDto } from './dto/registration.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() loginDTO: LoginDto) {
    return this.authService.login(loginDTO);
  }

  @Post('registration')
  async registration(@Body() registrationDTO: RegistrationDto) {
    await this.authService.registration(registrationDTO);
    return { message: `User [${registrationDTO.email}] created successfully` };
  }
}
