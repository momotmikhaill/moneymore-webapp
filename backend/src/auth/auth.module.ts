import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { IJWTConfig } from '../configuration/jwt.config';
import { JWTGuard } from './guards/jwt.guard';
import { JWTStrategy } from './strategies/jwt.strategy';

@Module({
  controllers: [AuthController],
  providers: [
    AuthService,
    /** Guards **/
    JWTGuard,
    /** Strategies **/
    JWTStrategy,
  ],
  imports: [
    UserModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => {
        const jwtConfig = config.get<IJWTConfig>('jwt');
        return {
          secret: jwtConfig.secret,
          signOptions: {
            expiresIn: jwtConfig.expireIn,
          },
        };
      },
      inject: [ConfigService],
    }),
    ConfigModule,
  ],
})
export class AuthModule {}
