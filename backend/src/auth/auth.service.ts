import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { RegistrationDto } from './dto/registration.dto';
import { User } from '../user/entities/user.entity';
import { IJWTPayload } from './interfaces/jwt-payload.interface';

import * as crypto from 'crypto';

const siteWideSalt = 'z2e608d08-bf7f-485a-9b03-08539bf6142c';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService, private readonly jwtService: JwtService) {}

  async login(loginDto: LoginDto) {
    const passwordHash = await this.encryptPassword(loginDto.email, loginDto.password);
    let user;
    try {
      user = await this.userService.findOneByEmailAndPassword(loginDto.email, passwordHash);
    } catch (e) {
      throw new UnauthorizedException('Email or password is invalid');
    }

    const accessToken = await this.generateAccessToken(user);

    return {
      accessToken,
    };
  }

  async registration(registrationDto: RegistrationDto): Promise<void> {
    let user;
    try {
      user = await this.userService.findOneByEmail(registrationDto.email);
    } catch (e) {}

    if (user) {
      throw new BadRequestException(`User [${registrationDto.email}] has already exists`);
    }

    const hashedPassword = await this.encryptPassword(registrationDto.email, registrationDto.password);
    const loweredEmail = String(registrationDto.email).toLowerCase();

    await this.userService.create({
      name: registrationDto.name,
      email: loweredEmail,
      password: hashedPassword,
    });
  }

  private async generateAccessToken(user: User): Promise<string> {
    const payload: IJWTPayload = { email: user.email, sub: user._id };
    console.log(payload);
    const token = await this.jwtService.signAsync(payload);

    return token;
  }

  private async encryptPassword(email: string, password: string): Promise<string> {
    const loweredEmail = String(email).toLowerCase();

    return new Promise((resolve, reject) => {
      crypto.pbkdf2(password, siteWideSalt + loweredEmail, 10000, 256, 'sha256', (err, key) => {
        if (err) {
          reject({ code: 'ENCRYPTION_ERROR', description: 'Error encrypting the password.', rawError: err });
        } else {
          resolve(key.toString('hex'));
        }
      });
    });
  }
}
