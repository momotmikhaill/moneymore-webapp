import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../../user/user.service';
import { ConfigService } from '@nestjs/config';
import { User } from '../../user/entities/user.entity';
import { IJWTConfig } from '../../configuration/jwt.config';
import { IJWTPayload } from '../interfaces/jwt-payload.interface';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService, private config: ConfigService) {
    const jwtConfig = config.get<IJWTConfig>('jwt');

    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConfig.secret,
    });
  }

  async validate(payload: IJWTPayload): Promise<User> {
    const userId = payload.sub;

    const user = await this.userService.findOneById(userId);

    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
