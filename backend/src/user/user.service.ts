import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './entities/user.entity';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async findManyByFilters(): Promise<User[]> {
    const users = await this.userModel.find().exec();
    return users;
  }

  async findOneById(id: string): Promise<User> {
    const result = await this.userModel.findById(id).exec();
    if (!result) {
      throw new NotFoundException(`User not found`);
    }

    return result;
  }

  async findOneByEmail(email: string): Promise<User> {
    const result = await this.userModel.findOne({ email: email }).exec();
    if (!result) {
      throw new NotFoundException(`User not found`);
    }

    return result;
  }

  async findOneByEmailAndPassword(email: string, passwordHash: string): Promise<User> {
    const result = await this.userModel.findOne({ email: email, password: passwordHash }).exec();
    if (!result) {
      throw new NotFoundException(`User not found`);
    }

    return result;
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const newUser = new this.userModel(createUserDto);
    const result = await newUser.save();

    return result;
  }
}
