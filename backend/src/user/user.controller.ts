import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  async getMany(): Promise<User[]> {
    const data = await this.userService.findManyByFilters();
    return data;
  }

  @Get('/:id')
  async getOne(@Param('id') id: string): Promise<User> {
    const data = await this.userService.findOneById(id);
    return data;
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    const result = await this.userService.create(createUserDto);
    return result;
  }
}
