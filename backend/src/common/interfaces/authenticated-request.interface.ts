import { User } from '../../user/entities/user.entity';

export interface AuthenticatedRequestInterface extends Request {
  user: User;
}
