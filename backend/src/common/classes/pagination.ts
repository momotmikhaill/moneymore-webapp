import { ApiProperty } from '@nestjs/swagger';

export class Pagination<T> {
  @ApiProperty({ type: Number, example: 1 })
  page: number;

  @ApiProperty({ type: Number, example: 50 })
  limit: number;

  @ApiProperty({ type: Number, example: 1 })
  totalCount: number;

  entities: T[];
}
