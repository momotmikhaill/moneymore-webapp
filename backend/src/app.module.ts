import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { TransactionModule } from './transaction/transaction.module';
import { AccountModule } from './account/account.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { MongooseModuleOptions } from '@nestjs/mongoose/dist/interfaces/mongoose-options.interface';

import databaseConfig, { IDatabaseConfig } from './configuration/database.config';
import { HealthCheckController } from './health-check/health-check.controller';
import { HealthCheckModule } from './health-check/health-check.module';
import appConfig from './configuration/app.config';
import jwtConfig from './configuration/jwt.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig, databaseConfig, jwtConfig],
    }),
    MongooseModule.forRootAsync({
      useFactory: (config: ConfigService): MongooseModuleOptions => {
        const dbConfig = config.get<IDatabaseConfig>('database');
        return {
          uri: `${dbConfig.prefix}://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}/?retryWrites=true&w=majority`,
        };
      },
      inject: [ConfigService],
      imports: [ConfigModule],
    }),
    ServeStaticModule.forRoot({
      rootPath: __dirname + '/public',
    }),
    AuthModule,
    UserModule,
    AccountModule,
    TransactionModule,
    HealthCheckModule,
  ],
  providers: [ClassSerializerInterceptor],
  controllers: [HealthCheckController],
})
export class AppModule {}
