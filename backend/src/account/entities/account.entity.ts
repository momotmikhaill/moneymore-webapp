import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { AccountTypeEnum } from '../enum/account-type.enum';
import { User } from '../../user/entities/user.entity';

@Schema()
export class Account extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  balance: number;

  @Prop({ required: true, enum: AccountTypeEnum })
  type: AccountTypeEnum;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name, required: true })
  ownerId: mongoose.Schema.Types.ObjectId;
}

export const AccountSchema = SchemaFactory.createForClass(Account);
