import { IsIn, IsNumber, IsString, Max, MaxLength, Min, MinLength } from 'class-validator';
import { ACCOUNT_TYPES, AccountTypeEnum } from '../enum/account-type.enum';

export class CreateAccountDto {
  @IsString()
  @MinLength(1)
  @MaxLength(255)
  name: string;

  @IsNumber()
  @Min(0)
  @Max(9999999)
  balance: number;

  @IsString()
  @IsIn(ACCOUNT_TYPES, {
    message: `type has to be one of the following values: ${ACCOUNT_TYPES.join(',')}`,
  })
  type: AccountTypeEnum;
}
