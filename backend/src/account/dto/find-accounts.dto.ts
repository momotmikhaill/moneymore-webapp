import { ACCOUNT_TYPES, AccountTypeEnum } from '../enum/account-type.enum';
import { IsIn, IsOptional } from 'class-validator';

export class FindAccountsDto {
  @IsOptional()
  @IsIn(ACCOUNT_TYPES, {
    message: `type has to be one of the following values: ${ACCOUNT_TYPES.join(',')}`,
  })
  type?: AccountTypeEnum;
}
