import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Account } from './entities/account.entity';
import { CreateAccountDto } from './dto/create-account.dto';
import { FindAccountsDto } from './dto/find-accounts.dto';
import { UpdateAccountDto } from './dto/update-account.dto';

@Injectable()
export class AccountService {
  constructor(@InjectModel(Account.name) private accountModel: Model<Account>) {}

  async findManyByOwnerIdAndFilters(userId: string, filters: FindAccountsDto): Promise<Account[]> {
    const query: Record<string, any> = { owner: userId };
    if (filters.type) {
      query.type = filters.type;
    }
    const result = await this.accountModel.find(query).exec();

    return result;
  }

  async findOneByOwnerIdAndId(userId: string, id: string): Promise<Account> {
    const result = await this.accountModel.findOne({ _id: id, owner: userId }).exec();
    if (!result) {
      throw new NotFoundException(`Account not found`);
    }

    return result;
  }

  async create(userId: string, createAccountDto: CreateAccountDto): Promise<Account> {
    const newAcc = new this.accountModel({
      ...createAccountDto,
      ownerId: userId,
    });
    const result = await newAcc.save();

    return result;
  }

  async update(id: string, updateAccountDto: UpdateAccountDto): Promise<Account> {
    const result = await this.accountModel.findByIdAndUpdate(id, updateAccountDto, { new: true });
    if (!result) {
      throw new NotFoundException(`Account ${id} not found`);
    }
    return result;
  }
}
