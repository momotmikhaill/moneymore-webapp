export enum AccountTypeEnum {
  WALLET = 'wallet',
  INCOME = 'income',
  EXPENSE = 'expense',
}

export const ACCOUNT_TYPES = [AccountTypeEnum.INCOME, AccountTypeEnum.WALLET, AccountTypeEnum.EXPENSE];
