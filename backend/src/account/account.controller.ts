import { Body, Controller, Get, Param, Post, Query, Req, UseGuards } from '@nestjs/common';
import { AccountService } from './account.service';
import { JWTGuard } from '../auth/guards/jwt.guard';
import { AuthenticatedRequestInterface } from '../common/interfaces/authenticated-request.interface';
import { CreateAccountDto } from './dto/create-account.dto';
import { Account } from './entities/account.entity';
import { FindAccountsDto } from './dto/find-accounts.dto';

@Controller('accounts')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Get()
  @UseGuards(JWTGuard)
  async getMany(@Req() { user }: AuthenticatedRequestInterface, @Query() filters: FindAccountsDto) {
    const accounts = await this.accountService.findManyByOwnerIdAndFilters(user._id, filters);
    return accounts;
  }

  @Get(':id')
  @UseGuards(JWTGuard)
  async getOne(@Req() { user }: AuthenticatedRequestInterface, @Param('id') id: string): Promise<Account> {
    const account = await this.accountService.findOneByOwnerIdAndId(user._id, id);
    return account;
  }

  @Post()
  @UseGuards(JWTGuard)
  async create(@Req() { user }: AuthenticatedRequestInterface, @Body() createAccountDto: CreateAccountDto) {
    const account = await this.accountService.create(user._id, createAccountDto);
    return account;
  }
}
