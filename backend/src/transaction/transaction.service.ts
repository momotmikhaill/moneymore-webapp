import { BadRequestException, forwardRef, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { Transaction } from './entity/transaction.entity';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { AccountService } from '../account/account.service';
import { AccountTypeEnum } from '../account/enum/account-type.enum';

@Injectable()
export class TransactionService {
  constructor(
    @Inject(forwardRef(() => AccountService)) private readonly accountService: AccountService,
    @InjectModel(Transaction.name) private readonly transactionModel: Model<Transaction>,
    @InjectConnection() private readonly mongoConnection: Connection,
  ) {}

  async findManyByOwnerIdAndFilters(userId: string): Promise<Transaction[]> {
    const query: Record<string, any> = { owner: userId };
    const result = await this.transactionModel.find(query).exec();

    return result;
  }

  async findOneByOwnerIdAndId(userId: string, id: string): Promise<Transaction> {
    const result = await this.transactionModel.findOne({ _id: id, owner: userId }).exec();
    if (!result) {
      throw new NotFoundException(`Account not found`);
    }

    return result;
  }

  async create(userId: string, createTransactionDto: CreateTransactionDto): Promise<Transaction> {
    const accountFrom = await this.accountService.findOneByOwnerIdAndId(userId, createTransactionDto.fromId);
    const accountTo = await this.accountService.findOneByOwnerIdAndId(userId, createTransactionDto.toId);

    if (accountFrom._id === accountTo._id) {
      throw new BadRequestException();
    }

    if (
      (accountFrom.type === AccountTypeEnum.EXPENSE && (accountTo.type === AccountTypeEnum.WALLET || accountTo.type === AccountTypeEnum.INCOME || accountFrom.type === AccountTypeEnum.EXPENSE)) ||
      (accountFrom.type === AccountTypeEnum.WALLET && accountTo.type === AccountTypeEnum.INCOME) ||
      (accountFrom.type === AccountTypeEnum.INCOME && (accountTo.type === AccountTypeEnum.INCOME || accountTo.type === AccountTypeEnum.EXPENSE))
    ) {
      throw new BadRequestException();
    }

    accountFrom.balance -= createTransactionDto.amount;
    accountTo.balance += createTransactionDto.amount;

    const session = await this.mongoConnection.startSession();
    session.startTransaction();
    try {
      const newAcc = new this.transactionModel({
        fromId: accountFrom._id,
        toId: accountTo._id,
        ownerId: userId,
        amount: createTransactionDto.amount,
        date: new Date(createTransactionDto.date),
      });

      await Promise.all([this.accountService.update(accountFrom._id, accountFrom), this.accountService.update(accountTo._id, accountTo)]);

      const result = await newAcc.save();

      return result;
    } catch (e) {
      await session.abortTransaction();
    } finally {
      await session.endSession();
    }
  }
}
