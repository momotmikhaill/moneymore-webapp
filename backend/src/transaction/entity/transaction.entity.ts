import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from '../../user/entities/user.entity';
import { Account } from '../../account/entities/account.entity';
import mongoose, { Document } from 'mongoose';

@Schema()
export class Transaction extends Document {
  @Prop({ required: true })
  amount: number;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name, required: true })
  ownerId: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: Account.name, required: true })
  fromId: mongoose.Schema.Types.ObjectId;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: Account.name, required: true })
  toId: mongoose.Schema.Types.ObjectId;

  @Prop({ type: Date, required: true })
  date: Date;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
