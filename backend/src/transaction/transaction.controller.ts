import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { JWTGuard } from '../auth/guards/jwt.guard';
import { AuthenticatedRequestInterface } from '../common/interfaces/authenticated-request.interface';
import { Transaction } from './entity/transaction.entity';
import { CreateTransactionDto } from './dto/create-transaction.dto';

@Controller('transactions')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Get()
  @UseGuards(JWTGuard)
  async getMany(@Req() { user }: AuthenticatedRequestInterface): Promise<Transaction[]> {
    const result = await this.transactionService.findManyByOwnerIdAndFilters(user._id);
    return result;
  }

  @Post()
  @UseGuards(JWTGuard)
  async create(@Req() { user }: AuthenticatedRequestInterface, @Body() createTransactionDto: CreateTransactionDto) {
    const result = await this.transactionService.create(user._id, createTransactionDto);
    return result;
  }
}
