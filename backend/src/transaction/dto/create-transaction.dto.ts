import { IsISO8601, IsNumber, IsPositive, IsString } from 'class-validator';

export class CreateTransactionDto {
  @IsNumber()
  @IsNumber()
  @IsPositive()
  amount: number;

  @IsString()
  fromId: string;

  @IsString()
  toId: string;

  @IsISO8601()
  date: string;
}
