# docker build -t "moneymore-image" -f "Dockerfile" --no-cache .
# docker run --name moneymore-container -d -p 7777:7777 --env-file backend/.env moneymore-image

FROM node:16.14.2-alpine
RUN apk update && apk add --no-cache bash g++ gcc libgcc libstdc++ linux-headers make python3 git fts-dev

# Copy folder
RUN mkdir app
COPY . /app
WORKDIR /app

#Instal project dependencies
RUN bash bin/install.bash

#Install NestJS CLI and Angular CLI
RUN bash bin/setup-environment.bash

#Build frontend and backend
RUN npm run build

EXPOSE 7777
CMD npm run server