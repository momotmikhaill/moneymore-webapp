import { Component, OnInit } from '@angular/core';
import { AccountService } from "../account/account.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  incomeAccounts: Record<string, any>[] = [];
  walletAccounts: Record<string, any>[] = [];
  expenseAccounts: Record<string, any>[] = [];

  constructor(private readonly accountService: AccountService) { }

  ngOnInit(): void {
    console.log('HomeComponent');
    this.accountService.getMany().subscribe((result) => {
      this.incomeAccounts = result.filter((entity: Record<string, any>) => entity['type'] === 'income');
      this.walletAccounts = result.filter((entity: Record<string, any>) => entity['type'] === 'wallet');
      this.expenseAccounts = result.filter((entity: Record<string, any>) => entity['type'] === 'expense');
    });
  }

}
