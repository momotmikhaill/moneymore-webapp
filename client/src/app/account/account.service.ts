import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({providedIn: 'any'})
export class AccountService {
  readonly apiURL = 'api/accounts';

  constructor(private http: HttpClient) {}

  getMany() {
    const devToggles = this.http.get<Record<string, any>[]>(`${this.apiURL}`);
    return devToggles;
  }

}
