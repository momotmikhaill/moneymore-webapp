import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  isFormSubmitted = false;
  isInvalidCredentials = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  ngOnInit(): void {
    console.log('LoginComponent');
    if (this.authService.getToken()) {
      this.router.navigate(['']);
      return ;
    }
    this.cleanErrors();
  }

  onInputChange(): void {
    this.isFormSubmitted = false;
  }

  async onSubmit(): Promise<void> {
    this.isFormSubmitted = true;
    if (this.form.valid) {
      const isAuthenticated = await this.authService.login(this.form.value.email, this.form.value.password);
      if (isAuthenticated) {
        this.cleanErrors();
        this.router.navigate(['']);
      } else {
        this.isInvalidCredentials = true;
      }
    }
  }

  private cleanErrors(): void {
    this.isFormSubmitted = false;
    this.isInvalidCredentials = false;
  }

}
