import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";

@Injectable({providedIn: 'root'})
export class AuthService {
  isAuthenticated: BehaviorSubject<boolean>;

  private accessToken: string|null;
  private readonly ACCESS_TOKEN = 'accessToken';


  constructor(private http: HttpClient) {
    this.accessToken = this.getToken();
    this.isAuthenticated = new BehaviorSubject<boolean>(!!this.accessToken )
  }

  async login(email: string, password: string): Promise<boolean> {
    let isAuthenticated = false;
    try {
      const response = await this.http.post<any>(`api/auth/login`, { email, password }).toPromise();
      if (response && response.accessToken) {
        this.setToken(response.accessToken);
        isAuthenticated = true;
        this.isAuthenticated.next(true);
      }
    } catch (e) {
      isAuthenticated = false;
    }

    return isAuthenticated;
  }

  logout(): void {
    this.accessToken = null;
    localStorage.removeItem(this.ACCESS_TOKEN);
    this.isAuthenticated.next(false);
  }

  getToken(): string|null {
    let token = this.accessToken;
    if (!token) {
      token = localStorage.getItem(this.ACCESS_TOKEN);
    }
    return token;
  }

  setToken(token: string): void {
    this.accessToken = token;
    localStorage.setItem(this.ACCESS_TOKEN, token);
  }
}
