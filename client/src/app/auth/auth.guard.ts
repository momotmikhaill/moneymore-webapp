import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'any'})
export class AuthGuard {
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const accessToken = this.authService.getToken();

    if (accessToken) {
      return true;
    }

    await this.router.navigate(['/auth/login']);
    return false;
  }
}
