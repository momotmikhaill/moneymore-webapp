import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isAuthenticated: boolean = false;

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.authService.isAuthenticated.subscribe((v) => {
      this.isAuthenticated = v;
    })
  }

  onLogoutClick(): void {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }

}
