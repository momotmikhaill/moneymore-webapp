import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from '../../auth/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const modifiedRequest = this.addTokenDataToRequest(request);
    return next.handle(modifiedRequest);
  }

  private addTokenDataToRequest(request: HttpRequest<any>): HttpRequest<any>{
    let modifiedRequest = request;
    const token = this.authService.getToken();
    if (token) {
      modifiedRequest = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return  modifiedRequest;
  }
}
